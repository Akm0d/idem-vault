.. idem-vault documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem-vault's Documentation!
======================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/idem-vault
   topics/secrets
   tutorial/index
   releases/index

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/saltstack/pop/pop-create/>

Indices and tables
==================

* :ref:`modindex`
